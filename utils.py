from web3 import Web3


host = ""  # ganache rpc server
chain_id = 1337  # ganache network id
address = ""  # first address in ganache accounts
private_key = ""  # private key of that address


def get_w3():
    w3 = Web3(Web3.HTTPProvider(host))
    return w3
