from utils import *
import json

contract_address = ''  # contract address in tx receipt
with open('abi.json') as f:
    abi = json.load(f)

w3 = get_w3()

counter = w3.eth.contract(address=contract_address, abi=abi)
nonce = w3.eth.getTransactionCount(address)

print(counter.functions.get().call())
store_tx = counter.functions.set(15).buildTransaction({
    "gasPrice": w3.eth.gas_price,
    "chainId": chain_id,
    "from": address,
    "nonce": nonce
})
signed_store_tx = w3.eth.account.sign_transaction(store_tx, private_key)

store_tx_hash = w3.eth.send_raw_transaction(signed_store_tx.rawTransaction)
print(counter.functions.get().call())
