## Install Ganache
## Start Env
- start Ganache
- change utils.py env accordingly
  - host
  - chain_id
  - address
  - private_key
## Deploy contract
```
python deploy.py
```
change contract address in interact.py base on receipt
## Interact with contract and see the difference
```
python interact.py
```