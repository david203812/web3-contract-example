from solcx import compile_standard
from utils import *

import json

with open(f"./Demo.sol") as f:
    contract_file = f.read()

# Compile Contract
compiled_sol = compile_standard(
    {
        "language": "Solidity",
        "sources": {f"Demo.sol": {"content": contract_file}},
        "settings": {
            "outputSelection": {
                "*": {"*": ["abi", "metadata", "evm.bytecode", "evm.sourceMap"]}
            },
        },
    },
    solc_version="0.6.0"
)

with open("compiled_code.json", "w") as file:
    json.dump(compiled_sol, file)

abi = compiled_sol['contracts']['Demo.sol']['Counter']['abi']
bytecode = compiled_sol['contracts']['Demo.sol']['Counter']['evm']['bytecode']['object']

with open("abi.json", "w") as file:
    json.dump(abi, file)


w3 = get_w3()

# Create contract
Contract = w3.eth.contract(abi=abi, bytecode=bytecode)

# Transaction
nonce = w3.eth.getTransactionCount(address)
# Build a tx
transaction = Contract.constructor().buildTransaction({
    "gasPrice": w3.eth.gas_price,
    "chainId": chain_id,
    "from": address,
    "nonce": nonce
})

# Sign a tx
signed_txn = w3.eth.account.sign_transaction(transaction, private_key)

# Send a tx
tx_hash = w3.eth.send_raw_transaction(signed_txn.rawTransaction)
print(f'========hash=======\n{tx_hash.hex()}')

# Get receipt
tx_receipt = w3.eth.wait_for_transaction_receipt(tx_hash)
print(f'========receipt=====\n{tx_receipt}')
